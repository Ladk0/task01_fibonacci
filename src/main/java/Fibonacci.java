public class Fibonacci {
    public int[] fib(int f1, int f2, int size) {
        int fibarr[] = new int[size];
        fibarr[0] = f1;
        fibarr[1] = f2;
        for (int i = 2; i < size; i++) {
            fibarr[i] = fibarr[i - 2] + fibarr[i - 1];
        }
        return fibarr;
    }
}
