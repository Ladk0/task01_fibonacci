import java.util.Scanner;

public class Main {
    public static void main(String args[]){
        Scanner in = new Scanner(System.in);
        OddOrEven oddOrEven=new OddOrEven();
        int start;
        int end;
        int sum=0;
        int size;
        int f1;
        int f2;
        int oddCounter=0;
        int[] arr;

        System.out.print("Enter a start of the interval: ");
        while(!in.hasNextInt()){
            System.out.println("Not a number!");
            in.next();
        }
        start = in.nextInt();
        System.out.print("Enter an end of the interval: ");
        while(!in.hasNextInt()){
            System.out.println("Not a number!");
            in.next();
        }
        end = in.nextInt();

        f2=end;
        f1=end-1;

        for(int i=start;i<=end;i++){
            if(oddOrEven.isEven(i)){
                System.out.print(i + " ");
                sum+=i;
            }
        } System.out.println();
        for(int i=end;i>=start;i--){
            if(oddOrEven.isOdd(i)){
                System.out.print(i + " ");
                sum+=i;
            }
        } System.out.println();

        System.out.print("Sum = " + sum + "\n" +
                "\nEnter a size of the fibonacci set: ");
        do {
            while (!in.hasNextInt()) {
                System.out.println("Not a number!");
                in.next();
            }
            size = in.nextInt();
        }while(size<=2);

        arr=new Fibonacci().fib(f1, f2, size);

        for(int i=0;i<arr.length;i++) {
            if (new OddOrEven().isOdd(arr[i]))
                oddCounter++;
        }

        System.out.println("Odd numbers percentage is: " + 
		(float)oddCounter/arr.length*100 + "%, and even is: " + (100.f-((float)oddCounter/arr.length*100)) + "%.");
    }
}
